const fetch = require('node-fetch');

class Command {
	constructor(){
		this.handle = "mcstatus";
		this.description = "Get the status of a minecraft server.";
	}
	async run(data){
    	if (!data.args[0]) {
    		data.message.reply(`You need to specify a server ip! Example: ${data.parent.prefix}${this.handle} <server_ip>`);
    		return;
    	}

		var args = data.args[0].replace("http://", "").replace("https://", "").split(":");

		fetch(`https://mcapi.us/server/status?ip=${args[0]}&port=${args[1]||25565}`)
	    .then(res => res.json())
	    .then(json => {
	    	if (json.error != '') {
	    		data.message.reply(json.error);
	    		return;
	    	}

	    	data.message.reply(`**\`${args[0]}\`** ${json.online?"Online!":"Offline."} ${json.players.now}/${json.players.max} players. ${json.server.name!=''?', running '+json.server.name:''}.`);
	    });
	}
}

module.exports = Command;