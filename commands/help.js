class Command {
	constructor(){
		this.handle = "help";
		this.description = "Get a summary of available commands.";
	}
	async run(data){
		var commandString = "";

		for (var i = 0; i < data.parent.commands.length; i++) {
			if (commandString.length >= 4500) break;
			commandString += `**${data.parent.prefix}${data.parent.commands[i].handle}:** ${data.parent.commands[i].description||""}\n`;
		}

		data.message.channel.send(commandString);
	}
}

module.exports = Command;