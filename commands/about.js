class Command {
	constructor(){
		this.handle = "about";
		this.description = "I can tell you about more about myself.";
	}
	async run(data){
		var items = [
			'love',
			'boredom',
			'angst',
			'fear',
			'js'
		];

		var noun = items[Math.floor(Math.random() * items.length)];

		data.message.reply(`Made with ${noun} by <@6603587622222696448>.`);
	}
}

module.exports = Command;