class Command {
	constructor(){
		this.handle = "ping";
		this.description = "Ping? Pong!";
	}
	async run(data){
		data.message.reply("Pong!");
	}
}

module.exports = Command;