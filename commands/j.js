Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

String.prototype.nopunct = function(){
	return this.replace(/[^A-Za-z0-9\s]/g,"").replace(/\s{2,}/g, " ");
}

class Room {
    constructor(properties) {
        this.name = properties.name;
        this.description = properties.description;
        this.manager = properties.manager;
        this.items = properties.items || [];
        this.directions = properties.directions || {};
        this.locked = properties.locked || false;
        this.props = properties.props || [];
        this.visited = false;
        this.status = properties.status || undefined;
        this.events = {};
        this.resetStatus = properties.resetStatus;
        if (this.resetStatus) {
            this.on('left', () => {
                this.status = properties.status;
            });
        }
    }
    itemList(){
    	//var items = this.items.map(o => o.name).join(', ');
    	var items = "";
    	for (var i = 0; i < this.items.length; i++) {
    		items += this.items[i].name;
    		if (i<this.items.length-2) items+= ", ";
    		else if (i<this.items.length-1) items+= " and ";
    	}
    	return items;
    }
    introduce(){
        this.emit('entered');
        this.visited = true;

        return `${this.status=='dark'?"Its pich-black here. You fumble in the dark, trying to get a grip of a wall to avoid falling.":this.description}\n\n${
        	this.items.length>0&&this.status!='dark'?'Looking around, you see ' + this.itemList() + '.':'There seems to be nothing of value here.'} ${this.listPlayers()}`;
    }
    listPlayers(){
    	if (this.manager.players.length<1) return "You don't see anyone around here.";
    	else{
    		var playerList = "Out of the corner of your eye you see ";

    		var playerNames = [];

    		for (var i = 0; i < this.manager.players.length; i++) {
    			if (this.manager.players[i].room.name == this.name) {
	    			if (this.manager.processingPlayer.id == this.manager.players[i].id) continue;

	    			playerNames.push(this.manager.players[i].name);
    			}
    		}

    		for (var i = 0; i < playerNames.length; i++) {
    			playerList += playerNames[i];

	    		if (i<playerNames.length-2) playerList+= ", ";
	    		else if (i<playerNames.length-1) playerList+= " and ";

    		}

    		return playerNames.length>0?playerList+".":"You don't see anyone around here.";
    	}
    }
    listItems(){
    	return `${this.items.length>0&&this.status!='dark'?`Looking around, you notice ${this.items.length>1?' a few items':'an item'}: ` + this.itemList() + '.':'There seems to be nothing of value here.'}`;
    }
    listProps(){
        var propString = "";

        if (this.props.length<=0 || this.status == 'dark') {
            propString += "There's nothing of note around here.";
        }
        else{
            if (this.props.length==1) propString += "Of note is ";
            else propString += "Of note are ";
            for (var i = 0; i < this.props.length; i++) {
                propString += this.props[i].name;
                if (i<this.props.length-2) propString+= ", ";
                else if (i<this.props.length-1) propString+= " and ";
            }
        }

        return `You throw your gaze around the stuff in the room. ${propString}.`;
    }
    findItem(itemName){
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].tags.includes(itemName)) return this.items[i];
        }
        return false;
    }
    findProp(propName){
        for (var i = 0; i < this.props.length; i++) {
            if (this.props[i].tags.includes(propName)) return this.props[i];
        }
        return false;
    }
    setDirection(dir, room){
    	this.directions[dir] = room;
    }
    on(type, func){
        this.events[type] = func;
    }
    emit(type){
        if(this.events[type]) this.events[type](this);
    }
    affectRoom(func){
        if (func != undefined) func(this);
    }
    useProp(){
        return;
    }
}

class Item {
    constructor(properties) {
        this.name = properties.name;
        this.type = properties.type;
        this.description = properties.description;
        this.key = properties.key;
        this.used = properties.used;
        this.recipe = properties.recipe;
        this.craft = properties.craft;
        this.action = properties.action;
        this.events = {};
        this.tags = properties.tags || [];
        this.affectRoom = properties.affectRoom;
        this.destroyIngredients = properties.destroyIngredients;
    }
    useProp(){
        if (this.type=='prop') return this.action;
    }
    on(type, func){
        this.events[type] = func;
    }
    emit(type){
        if(this.events[type]) this.events[type](this);
    }
}

class Recipe {
    constructor(properties) {
        this.item1 = properties.item1;
        this.item = properties.item2;
        this.result = properties.result;
    }
}

var craftables = [];

craftables.push(new Item({
    name: 'an old lighter',
    type: 'tool',
    description: 'This lighter stinks of kerosene. Its got enough fuel for now.',
    destroyIngredients: true,
    recipe: {
        item1: 'a small can of kerosene',
        item2: 'a dented lighter'
    },
    tags: ['lighter', 'old lighter'],
    used: 'You flip the ligher open and swipe your finger down the spark wheel. After a few attemps, a faint flame appears, quickly rising to a lively blaze. You feel strangely relieved.',
    affectRoom: function(e){
        e.status = undefined;
    },
    craft: 'The rancid smell of old kerosene fills the air as you pour whatever was left from the can into the lighter.'
}));

var boardedWindow = new Item({
    name: 'a boarded up window',
    type: 'prop',
    tags: ['window', 'boarded window', 'boarded up window'],
    description: "This window is nailed shut. There is no way you can pry these huge boards off with your bare hands.",
    action: "You pause and stare intently at the window. The bleak rays of sunlight pulse with each passing cloud."
});

class Player {
    constructor(properties) {
    	this.id = properties.data.message.author.id;
    	this.name = properties.data.message.author.username;
    	this.room = properties.room;
    	this.message = properties.data.message;

        this.inventory = [
            new Item({
                name: 'pocket lint',
                type: 'fuel',
                tags: ['lint'],
                description: `If there's one thing you will never run out of, its this.`
            })
        ];
    }
    craft(craftables, item1, item2){
        for (var i = 0; i<craftables.length; i++) {
            if (craftables[i].recipe.item1 == item1.name && craftables[i].recipe.item2 == item2.name) {
                this.message.reply(`${craftables[i].craft}`);

                if (craftables[i].destroyIngredients) {
                    this.inventory.remove(item1);
                    this.inventory.remove(item2);
                }
                this.inventory.push(craftables[i]);

                return;
            }
            else{
                this.message.reply(`You squeeze ${item1.name} and ${item2.name} together. Nothing happens. Try using some glue next time.`);
            }
        }
    }
    use(item){
        if (!item.used) {
            this.message.reply(`You frantically turn ${item.name} around in your hands, realizing you forgot how to use it.`);
            return;
        }
        this.message.reply(`${item.used}`);

        if (item.affectRoom != undefined) {
            this.room.affectRoom(item.affectRoom);
        }

        if (item.type=="food" || item.type == "consumable") this.inventory.remove(item);
    }
    pickUp(item){
        if (!item) {
            this.message.reply(`You reach out towards the item you saw. As you approach and squint you realize there was no item.`);
            return;
        }

        this.inventory.push(item);
        item.emit('pickedup');
        this.room.items.remove(item);

        this.message.reply(` You picked up ${item.name}. ${item.description}`);
    }
    findItem(itemName){
        for (var i = 0; i < this.inventory.length; i++) {
            if (this.inventory[i].tags.includes(itemName)) return this.inventory[i];
        }
        return false;
    }
    introduceRoom(){
    	this.message.reply(this.room.introduce());
    }
    listRoomProps(){
        this.message.reply(this.room.listProps());
    }
    itemList(){
        var items = "";
        for (var i = 0; i < this.inventory.length; i++) {
            items += this.inventory[i].name;
            if (i<this.inventory.length-2) items+= ", ";
            else if (i<this.inventory.length-1) items+= " and ";
        }
        return items;
    }
    listItems(){
        this.message.reply(`You stick your hand into your pockets and find ${this.inventory.length>0?this.itemList():'absolutely nothing. Not even pocket lint'}.`);
    }
    listRoomItems(){
    	this.message.reply(this.room.listItems());
    }
    listPlayers(){
    	this.message.reply(this.room.listPlayers());
    }
    enter (room){
        if (room.locked && this.inventory.filter(obj => { return obj.key == room.locked}).length <= 0) {
           	this.message.reply(`This room is locked.`);
            return;
        }
        else if (room.locked && this.inventory.filter(obj => { return obj.key == room.locked}).length > 0) {
           	this.message.reply(`This room is locked. You search your pockets for a key and find one that fits. Carefully, you unlock the door and enter.`);
            room.locked = false;
        }
        this.room.emit('left');
        this.room = room;
        this.introduceRoom();
    }
}

class GameManager {
	constructor(properties) {
		this.players = [];
		this.command;
		this.processingPlayer;
        this.lastMsg;
	}
	setCommand(data){
		this.command = data;
	}
    setLast(msg){
        this.lastMsg = msg;
    }
	listPlayers(data){
		var playerString = ``;

		for (var i = 0; i < this.players.length; i++) {
			playerString += this.players[i].name;
		}

		data.message.reply("List of people playing right now:\n" + playerString);
	}
	currentPlayer(data){
		for (var i = 0; i < this.players.length; i++) {
			if (this.players[i].id == data.message.author.id) {
				return this.players[i];
			}
		}
		//return error if player not registered
		data.message.reply("You are not yet playing! Try: " + data.parent.prefix + this.command.handle + " start");
	}
	addPlayer(data){
		for (var i = 0; i < this.players.length; i++) {
			if (this.players[i].id == data.message.author.id) {
				this.players.remove(this.players[i]);

				this.players.push(new Player({
					data: data,
					room: startingRoom
				}));

				return;
			}
		}

		this.players.push(new Player({
			data: data,
			room: startingRoom
		}));

		this.currentPlayer(data).introduceRoom();
	}
	run(data){
        this.setLast(data.message);
		this.processingPlayer = data.message.author;
        var commandString = data.args.join(" ");

		if (commandString.startsWith('start')) {
			this.addPlayer(data);
            return;
		}

        var player = this.currentPlayer(data);

        if (player) {
            if (commandString.startsWith('players')) {
                this.listPlayers(data);
            }
            else if (commandString.startsWith("inventory")) {
                player.listItems();
            }
            else if (commandString.startsWith("look around")) {
                player.listRoomProps();
            }
            else if (commandString.startsWith("admire")) {
                commandString = commandString.replace("admire", "").nopunct().trim();

                if (player.room.props.find(x => x.name.nopunct() === commandString)) {
                    data.message.reply(player.room.props.find(x => x.name.nopunct() === commandString).useProp());
                }
                else if (player.room.findProp(commandString)) {
                    data.message.reply(player.room.findProp(commandString).useProp());
                }
                else{
                    data.message.reply(`You stare blankly into the distance. After musing for a couple moments you return to the where you left off.`);
                }
            }
            else if (commandString.startsWith("pick up")) {
                commandString = commandString.replace("pick up", "").nopunct().trim();
                //maybe pick up by tags instead, hmm?
                if (player.room.items.find(x => x.name.nopunct() === commandString)) player.pickUp(player.room.items.find(x => x.name.nopunct() === commandString));
                else player.pickUp(player.room.findItem(commandString));
            }
            else if (commandString.startsWith("drop")) {
                commandString = commandString.replace("drop", "").nopunct().trim();

                var subjectItem = player.inventory.find(x => x.name.nopunct() === commandString) || player.findItem(commandString);
                
                if (subjectItem) {
                    player.inventory.remove(subjectItem);
                    player.room.items.push(subjectItem);

                    data.message.reply(`You pull ${subjectItem.name} out of your pocket and watch it make it's way to the floor below.`);
                    return;
                }

                data.message.reply(`You turn your pockets inside out but nothing falls out. Go figure.`);
            }
            else if (commandString.startsWith("use")) {
                commandString = commandString.replace("use", "").nopunct().trim();

                if (commandString.includes(' on ')) {
                    commandString = commandString.split(' on ');

                    var item1 = player.inventory.find(x => x.name.nopunct() === commandString[0]);
                    var item2 = player.inventory.find(x => x.name.nopunct() === commandString[1]);

                    if (!item1) {
                        item1 = player.findItem(commandString[0]);
                        if (!item1) {
                            data.message.reply(`You briefly dream of all kinds of cool things you could make with "${commandString[0]}". You feel a bit disappointed that you dont have it right now.`);
                            return;
                        }
                    }
                    if (!item2) {
                        item2 = player.findItem(commandString[1]);
                        if (!item2) {
                            data.message.reply(`You briefly dream of all kinds of cool things you could make with "${commandString[1]}". You feel a bit disappointed that you dont have it right now.`);
                            return;
                        }
                    }

                    if (item1==item2) {
                        data.message.reply(`You pull out ${item1.name} and then search your pockets, realizing you only have one of these. Oops.`);
                        return;
                    }

                    player.craft(craftables, item1, item2);
                }
                else{
                    if (player.inventory.find(x => x.name.nopunct() === commandString)) {
                        player.use(player.inventory.find(x => x.name.nopunct() === commandString));
                    }
                    else if (player.findItem(commandString)) {
                        player.use(player.findItem(commandString));
                    }
                    else{
                        data.message.reply(`You attempt to make use of "${commandString}". After failing to catch grip of your imagination, you give up.`);
                    }

                }
            }
            else if (commandString.startsWith('others')) {
                player.listPlayers();
            }
            else if (commandString.startsWith('search')) {
                player.listRoomItems();
            }
            else if (commandString.startsWith('where')) {
                data.message.reply("You are currently in the " + player.room.name);
            }
            else if (commandString.startsWith("go")) {
                var msg = data.args[1].nopunct().trim().replace('right', 'east').replace('forward', 'north').replace('left', 'west').replace('backward', 'south');

                if (player.room.directions[msg]) player.enter(player.room.directions[msg]);
                else data.message.reply(`There seems to be no exit this way.`);
            }
            else if (commandString.startsWith("help")) {
                data.message.reply(`You rummage in your pockets and find a tiny crumpled paper. Straightening it out, you see some writing on it:\n
**start** - create your player and start an adventure
**players** - see who else is playing
**others** - there is someone else here, i know it
**pick up [object]** - pick up an object
**use [object]** - attempt to operate an object
**use [object] on [object]** - DIY has never been easier
**drop [object]** - litter the floor with your goodies
**admire [prop]** - can't miss that opportunity
**go [direction]** - leave in a specified direction
**search** - look around for usable objects
**inventory** - verify the contents of your pockets
**where** - where have you got yourself into
**help** - pray for the best`);
            }
            else data.message.reply("Unknown command. Try: " + data.parent.prefix + this.command.handle + " help");
        }
	}
}

var manager = new GameManager();

class Command {
	constructor(){
		this.handle = "j";
		this.description = "Play jake.";

		this.players = [];

		manager.setCommand(this);
	}
	async run(data){
		if (!data.args) {
			data.message.reply("Please specify a command parameter");
			return;
		}
		if (!data.args[0]) {
			data.message.reply("Please specify a command parameter");
			return;
		}
		else{
			manager.run(data);
		}
	}
}

var livingRoom = new Room({
    name: "Living room",
    description: "You find yourself in a room full of boxes. Although there is nothing left but boxes and a big chandelier, you feel this may have once been a living room. Some boxes probably have goodies in them.",
    manager: manager,
    items: [
        new Item({
            name: 'a small can of kerosene',
            type: 'tool',
            tags: ['small can of kerosene', 'can of kerosene', 'kerosene', 'kerosene can'],
            description: `This isn't your go-to solution for starting fires, but it will do.`
        }),
        new Item({
            name: 'a pack of saltine crackers',
            type: 'food',
            tags: ['pack of saltine crackers', 'saltine crackers', 'crackers', 'pack of crackers'],
            description: 'Who knows how long these have been here. Better make sure they still taste good.',
            used: 'You stuff your face full of dry, crunchy crackers. Before you realize, you finish the whole pack.'
        }),
        new Item({
            name: 'a blank notepad',
            type: 'note',
            tags: ['blank notepad', 'notepad'],
            description: 'This notepad seems to be entirely blank. Or maybe its full of invisible ink?' 
        })
    ],
    props: [
        new Item({
            name: "a dusty chandelier",
            tags: ['dusty chandelier', 'chandelier'],
            type: 'prop',
            description: "This chandelier is many times older than you, thats for sure.",
            action: "You look up at the chandelier. No matter how long you keep staring at it, it doesn't seem to want to fall. Oh well."
        })
    ]
});

var closet = new Room({
    name: "Closet",
    description: "You find yourself in a small, cramped space. The smell of damp dust is overpowering.",
    manager: manager,
    locked: 'closet_key',
    status: 'dark',
    resetStatus: true,
    items: [
        new Item({
            name: 'a snapped broom handle',
            type: 'tool',
            tags: ['snapped broom handle', 'broom handle', 'handle'],
            description: 'The broom is nowhere to be seen. Maybe some day you can reunite it with the handle.'
        }),
        new Item({
            name: 'a crumpled piece of paper',
            type: 'note',
            tags: ['crumpled piece of paper', 'piece of paper', 'paper', 'piece'],
            description: 'Its a piece of what probably used to be a notebook. Its smeared with dust and ink.',
            used: `You flip the piece of paper around in your hands and notice something on the back.

Its a hand drawn picture of a dog. Or maybe a cat. You're not quite sure of the artist's direction here. Under the drawing you see some text. It says "Leslie".`
        })
    ]
});

var street = new Room({
    name: "Street",
    description: "You're free.",
    manager: manager,
    locked: 'street_key'
});

var lobby = new Room({
    name: "Lobby",
    description: "You find yourself in a small, poorly lit room. Dim rays of sunshine filter through the large boarded up window.",
    manager: manager,
    directions: {
        north: street
    },
    props: [ boardedWindow ]
});

var diningRoom = new Room({
    name: "Dining room",
    description: "You find yourself in a large, spacious room. In the center there is a big table covered by a drabby tablecloth. You sure wish you could sit down and have a meal, but there's no food here, only chairs. And you don't eat chairs.",
    manager: manager,
    directions: {
        west: lobby
    }
});

lobby.setDirection('east', diningRoom);

var hall = new Room({
    name: "Hallway",
    description: "You find yourself in a long, gloomy hallway. The walls are stained with grime and dust and the air smells damp.",
    manager: manager,
    directions: {
        north: lobby,
        east: closet,
        south: livingRoom
    }
});

lobby.setDirection('south', hall);
closet.setDirection('west', hall);
livingRoom.setDirection('north', hall);

var startingRoom = new Room({
    name: "Strange room",
    description: "You find yourself in a strange room. There is barely any light filtering through the single boarded up window. To the right of you is a door.",
    manager: manager,
    items: [
        new Item({
            name: 'a rusty, pitted knife',
            type: 'tool',
            tags: ['rusty pitted knife', 'rusty knife', 'pitted knife', 'knife'],
            description: 'This "knife" is merely a blade. The handle was  broken off a long time ago.'
        }),
        new Item({
            name: 'a dented lighter',
            type: 'tool',
            tags: ['dented lighter', 'lighter'],
            description: 'This lighter is all out of fuel.'
        })
    ],
    directions: {
        east: hall,
    },
    props: [ boardedWindow ]
});

hall.setDirection('west', startingRoom);

var keyPickedUp = false;
var keyDropped = false;

hall.on('entered', function(e){
    if (e.visited && !keyDropped) {
        e.items.push(new Item({
            name: 'a crooked key',
            type: 'key',
            key: 'closet_key',
            tags: ['crooked key', 'key'],
            description: `Its a key that has been bent out of shape with it's frequent usage and age.`
        }));
        keyDropped = true;
        e.items[e.items.length-1].on('pickedup', function(e){
            if (!keyPickedUp) {
                manager.lastMsg.reply(`
You could swear this hasn't been here before. 
You dart your eyes around the room nervously, expecting to see some boogeyman or perhaps a kooky victorian ghost, however theres still nothing but you and your doubts around. 
You shrug and shove the key into your pocket.`);
                keyPickedUp = true;
            }
        });
    }
});

module.exports = Command;

//make seeing players impossible in the dark