const Nertivia = require("nertivia.js");
const chokidar = require("chokidar");
const fs = require('fs');

const client = new Nertivia.Client();
const config = require("./config.json");

function requireUncached(module) {
    delete require.cache[require.resolve(module)];
    return require(module);
}

class CommandProcessor{
	constructor(args){
		this.prefix = args.prefix;
    	this.commands = [];
    	this.commandDir = args.commandDir;
	}
	runCommand(commandData, command){
		if (this.commands.length<1) {
	      	commandData.message.reply("No commands loaded!");
	      	return;
		}
	    for (var i = 0; i < this.commands.length; i++) {
	      if (this.commands[i].handle == command) {
	          this.commands[i].run(commandData);
	          break;
	      }

	      if (i == this.commands.length-1) {
	      	commandData.message.reply("Command not found :(");
	      }
	    }
	}
	handle(message){
		if (!message.author || message.author.id == client.user.id) return;

		if(!message.content || message.content.indexOf(this.prefix) !== 0) return;

		try{
			const args = message.content.slice(this.prefix.length).trim().split(/ +/g);
			const command = args.shift().toLowerCase();

		    this.runCommand({ message: message, client: client, parent: this, args: args }, command);
		}
		catch (e){
			console.log("Error: ", e);
		}
	}
	loadCommands(){
    	this.commands = [];
    	
	    fs.readdir(this.commandDir, (err, files) => {
			files.forEach(file => {
				var item = new requireUncached(this.commandDir + file);
				this.commands.push(new item());
			});
	    });
	    console.log("Loaded commands.");
	}
}

var mp = new CommandProcessor({
	prefix: config.prefix,
	commandDir: config.commandDir
});

var reloadTimeout;
chokidar.watch(mp.commandDir).on('all', (event, path) => {
	clearTimeout(reloadTimeout);
	reloadTimeout = setTimeout((argument) => {
		mp.loadCommands();
	}, 1500);
});

client.on("ready", () => {
    console.log(`Logged in as ${client.user.tag}!`);
});
 
client.on("message", msg => {
	mp.handle(msg);
});
 
client.login(config.token);